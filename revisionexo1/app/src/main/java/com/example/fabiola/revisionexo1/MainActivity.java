package com.example.fabiola.revisionexo1;

import android.content.Intent;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private CoverFlowAdapter adapter;

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        String donnees = "";
        JSONObject jsonObject = null;

        //conversion fichier en string
        try {
            donnees = utf8FileToString(new File("test_database_veegup.json"));
            jsonObject = new JSONObject(donnees);

            //recuperation de la liste
            JSONArray array = new JSONArray(jsonObject);

            //recuperation des données dans la liste
            ArrayList<ArrayList<String>> all = new ArrayList<ArrayList<String>>();

            for (int i = 0; i < array.length(); i++) {
                JSONArray a = new JSONArray(array.getString(i));
                ArrayList<String> liste = new ArrayList<String>();
                for (int j = 0; j < a.length(); j++) {
                    JSONObject obj = new JSONObject(a.getString(j));
                    liste.add(obj.getString("avgNote"));
                    liste.add(obj.getString("img"));
                    liste.add(obj.getString("meal_cat"));
                    liste.add(obj.getString("nrNotes"));
                    liste.add(obj.getString("title"));

                }
                all.add(liste);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }




    public static String inputStreamToString(InputStream is, Charset charset) throws IOException {
        StringBuilder builder = new StringBuilder();
        BufferedReader reader = new BufferedReader(new InputStreamReader(is, charset));
        for(String line = reader.readLine(); line != null; line = reader.readLine()) {
            builder.append(line);
        }
        return builder.toString();
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public static String fileToString(File file, Charset charset) throws IOException {
        try(InputStream is = new FileInputStream(file)) {
            return inputStreamToString(is, charset);
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public static String utf8FileToString(File file) throws IOException {
        return fileToString(file, Charset.forName("utf-8"));
    }


}

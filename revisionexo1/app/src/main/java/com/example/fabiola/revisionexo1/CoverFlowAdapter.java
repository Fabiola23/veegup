package com.example.fabiola.revisionexo1;

import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by fabiola on 06/05/18.
 */

class CoverFlowAdapter extends BaseAdapter {

    private ArrayList<String> data; //les infos à afficher

    private AppCompatActivity activity;

    public CoverFlowAdapter(AppCompatActivity context, ArrayList<String> objects) {
        this.activity = context;
        this.data = objects;
    }

    @Override
    public int getCount() {
        return data.size();;
    }

    @Override
    public Object getItem(int i) {
        return data.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        return null;
    }

    private static class ViewHolder {
        private TextView gameName;
        private ImageView gameImage;

        public ViewHolder(View v) {
            gameImage = (ImageView) v.findViewById(R.id.image);
            //gameName = (TextView) v.findViewById(R.id.name);
        }
    }
}
